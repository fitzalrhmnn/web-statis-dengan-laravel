<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="first name"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="last name"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender">Male <br><br>
        <input type="radio" name="gender">Female <br><br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="WN">
            <option value="Indonesia"> Indonesia</option>
            <option value="Malaysia"> Malaysia</option>
            <option value="singapore"> Singapore</option>
            <option value="australian"> Australian</option>
        </select> <br><br>
        <label>Languange Spoken</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br><br>
        <input type="checkbox"> English <br><br>
        <input type="checkbox"> Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>